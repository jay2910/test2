const { adduserdata , putVaccine, updatelasthostpitalcode } = require('../db/querys/people')
const {availableslot ,addbookslot , addnewslot, removecount , removetotalVacsincode} = require('../db/querys/hospital')

const putData = async(ctx) => {
    const pno = ctx.request.body.pno
    const vno = ctx.request.body.vno
    const hno = ctx.request.body.hno

  
        const query = await adduserdata(pno,vno)         
        if(query){
            const checkslot = await availableslot(hno,vno)             
            if(checkslot){                
                    await addbookslot(hno,vno)
                    await updatelasthostpitalcode(pno,hno)
                    ctx.body = { message: "Your Slot Book Successfully" }           
            }else{                
                await addnewslot(hno,vno)
                await updatelasthostpitalcode(pno,hno)
                ctx.body = { message: "Your Slot Book Successfully" }
            } 
        }          
        
}

const doneVaccines = async(ctx) => {

    const pno = ctx.request.body.pno
    const vno = ctx.request.body.vno
    const hno = ctx.request.body.hno
 
        const query = await putVaccine(pno,vno,hno)  
        if(query){
            await removecount(hno, vno) 
            await removetotalVacsincode(hno, vno) 
            ctx.body = {message : "Vaccine Done successfully"}
        }  
    
}

module.exports = { putData , doneVaccines };