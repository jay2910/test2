const Myconnection = Database.collection('citizen');

const finduser = (pno) => Myconnection.findOne({phoneNo : pno})

const updatelasthostpitalcode = (pno, hno) => Myconnection.updateOne({phoneNo : pno}, { $set : {lastHospitalCode : hno} })

const alreadyBookSlot = (pno,vno,hno) => Myconnection.findOne({phoneNo : pno , lastHospitalCode : hno , 'vaccinations.code' : vno , 'vaccinations.isVaccinated' : false })

const alreadytakeVaccine = (pno,vno,hno) => Myconnection.findOne({phoneNo : pno , lastHospitalCode : hno ,  'vaccinations.code' : vno , 'vaccinations.isVaccinated' : true})

const adduserdata = (pno, vno) => Myconnection.updateOne({ phoneNo : pno },
    { $push : { "vaccinations" : {
        code : vno,
        date : new Date,
        isVaccinated : false
    }}})

const putVaccine = (pno, vno, hno) => Myconnection.updateOne({phoneNo : pno , 'vaccinations.code' : vno },
{$set : { 'vaccinations.$.isVaccinated' : true}})


module.exports = {finduser , adduserdata , updatelasthostpitalcode , alreadytakeVaccine , putVaccine , alreadyBookSlot}
