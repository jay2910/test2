const { finduser,  alreadyBookSlot , alreadytakeVaccine } = require('../db/querys/people')
const { findhospitals, totalVacsinStorageslot } = require('../db/querys/hospital')


const checkvalidations = async(ctx, next) => {
    const pno = ctx.request.body.pno
    const vno = ctx.request.body.vno
    const hno = ctx.request.body.hno

    if(!pno){
        ctx.status = 401
        ctx.body = { message: "Please Enter phone number" }
        return
    }else if(typeof(pno) === 'number'){
        ctx.status = 401
        ctx.body = { message: "Please Enter phone number in string" }
        return
    }else if(!vno){
        ctx.status = 401
        ctx.body = { message: "Please Enter vaccine code" }
        return
    }else if(typeof(vno) === 'string'){
        ctx.status = 401
        ctx.body = { message: "Please Enter vaccine code in number" }
        return
    }else if(!hno){
        ctx.status = 401
        ctx.body = { message: "Please Enter Hospital code" }
        return
    }else if(typeof(hno) === 'string'){
        ctx.body = { message: "Please Enter Hospital code in number" }
        return 
    }

    try{

        const checkuser = await finduser(pno)
        if(checkuser){
            const checkhospital = await findhospitals(hno)
             if(checkhospital){
                 const checktotalVacsinStorageslot = await totalVacsinStorageslot(hno,vno)
                 if(checktotalVacsinStorageslot){
                     return next()
                 }else{
                    ctx.status = 401
                    ctx.body = { message : "This vaccine not available in Hospital"}
                 }
            }else{
                ctx.status = 401
                ctx.body = { message : "Please Enter valid Hospital Id"}
             }
            }else{
            ctx.status = 401
            ctx.body = { message : "User Not found"}
        }
    }catch(e){
        console.log(e);
    }
}

const slotcheck = async (ctx, next) => {    
    const pno = ctx.request.body.pno
    const vno = ctx.request.body.vno
    const hno = ctx.request.body.hno
    try{
    const checkalreadyBookdata = await alreadyBookSlot(pno,vno,hno)
    if(checkalreadyBookdata) {
        ctx.status = 401
        ctx.body = { message : "Your Slot alredy book" }
    }else{
        return next()
    }
    }catch(e){
        console.log(e);
    }
}


const checkalreadyBookSlot = async (ctx, next) =>{

    const pno = ctx.request.body.pno
    const vno = ctx.request.body.vno
    const hno = ctx.request.body.hno

    try{        
            const checkalreadytakeVaccine = await alreadytakeVaccine(pno,vno,hno)            
            if(checkalreadytakeVaccine){                
            ctx.status = 401
            ctx.body = { message : "You Already Take Vaccine" }
            }else{
                return next()
            }       

    }catch(e){
        console.log(e);
    }
}

module.exports = {checkvalidations , slotcheck, checkalreadyBookSlot}